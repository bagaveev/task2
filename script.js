const loginForm = document.querySelector(".login-form");
const loginFormEmail = loginForm.querySelector(".login-form__email");
const loginFormPassword = loginForm.querySelector(".login-form__password");
const loginFormError = loginForm.querySelector(".login-form__error");
const loginFormSubmitButton = loginForm.querySelector(".button_submit");

const userProfile = document.querySelector(".user-profile");
const userProfileName = userProfile.querySelector(".user-profile__name");
const userProfileAvatar = userProfile.querySelector(".user-profile__avatar");
const userProfileLogoutButton = userProfile.querySelector(".button_logout");

async function login({email, password}) {
    return new Promise((resolve, reject) => {
        if (email === "user@example.com" && password === "mercdev") {
            resolve({
                name: "React Bootcamp",
                photoUrl: "https://picsum.photos/200"
            })
        } else {
            reject(new Error("Incorrect email or password"))
        }
    })
}

async function tryLogin() {
    const email = loginFormEmail.value;
    const password = loginFormPassword.value;

    disableLoginForm();
    hideLoginError();

    try {
        const user = await login({email, password});
        showUserProfile(user);
    } catch (error) {
        showLoginError(error.message);
    } finally {
        enableLoginForm();
    }
}

function showLoginValidationError() {
    loginFormEmail.classList.add("text-input_invalid");
    loginFormPassword.classList.add("text-input_invalid");
}

function hideLoginValidationError() {
    loginFormEmail.classList.remove("text-input_invalid");
    loginFormPassword.classList.remove("text-input_invalid");
}

function showLoginError(message) {
    loginFormError.innerText = message;
    loginFormError.removeAttribute("hidden");
    showLoginValidationError();
}

function hideLoginError() {
    loginFormError.setAttribute("hidden", "true");
    hideLoginValidationError();
}

function disableLoginForm() {
    loginFormEmail.setAttribute("disabled", "true");
    loginFormPassword.setAttribute("disabled", "true");
    loginFormSubmitButton.setAttribute("disabled", "true");
}

function enableLoginForm() {
    loginFormEmail.removeAttribute("disabled");
    loginFormPassword.removeAttribute("disabled");
    loginFormSubmitButton.removeAttribute("disabled");
}

function showUserProfile(user) {
    loginForm.setAttribute("hidden", "true");

    userProfileName.innerText = user.name;
    userProfileAvatar.setAttribute("src", user.photoUrl);
    userProfile.removeAttribute("hidden");
}

function logout() {
    window.location.reload();
}

loginFormSubmitButton.addEventListener("click", tryLogin);
loginFormEmail.addEventListener("input", hideLoginValidationError);
loginFormPassword.addEventListener("input", hideLoginValidationError);
userProfileLogoutButton.addEventListener("click", logout);
